## All current endpoints are in here to be looped through, new ones need to be added seperated by a space
for endpoint in rhode_island cbs new_jersey_wh mississippi_igt west_virginia_wh indiana_wh colorado_wh michigan_wh iowa_wh virginia_wh germany_wh tennessee_wh
do
    mkdir -p ./backup_lists/${endpoint}
	## Very clever ssh connection without having to deal with the inveitable failure of forgetting how remote command execution or scp works
	## Equivalent to the GET in swagger UI, curl requesting all data for the specified endpoint
	sshpass -p $JENKINS_PASSWORD ssh $JENKINS_USER@$JENKINS_HOST "curl -X GET --header 'Accept: application/json' --header \"endpointId: $endpoint\" 'http://gtp-ses-filtering-web-service.gtp-ses-filtering-service.eks03.trading.aws-eu-west-1.prod.williamhill.plc/market/patterns'" >  ./backup_lists/${endpoint}/filterBackup_${endpoint}_`date -I`.txt
	#cat ./backup_lists/filterBackup_${endpoint}*
done

## This doesn't work for incredibly obvious reasons.
find ./backup_lists -type f -mtime +7 -exec rm -f {} \;
